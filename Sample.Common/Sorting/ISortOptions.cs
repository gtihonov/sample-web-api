namespace Sample.Common.Sorting
{
    public interface ISortOptions<TSort>
        where TSort : struct
    {
        TSort SortBy { get; set; }
        SortOptionsDirection? SortDirection { get; set; }
    }
}