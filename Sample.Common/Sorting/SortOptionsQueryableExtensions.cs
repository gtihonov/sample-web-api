using System;
using System.Linq;
using System.Linq.Expressions;

namespace Sample.Common.Sorting
{
    public static class SortOptionsQueryableExtensions
    {
        public static IOrderedQueryable<T> SortWithOptions<T, TSort>(this IQueryable<T> source, IQueryableSortOptions<T, TSort> options)
            where T : class
            where TSort : struct
        {
            return options.SortQueryable(source);
        }

        public static IOrderedQueryable<T> OrderByWithOrderDirection<T, TKey>(
            this IQueryable<T> source,
            Expression<Func<T, TKey>> expression,
            SortOptionsDirection direction)
        {
            switch (direction)
            {
                case SortOptionsDirection.Ascend:
                    return source.OrderBy(expression);

                case SortOptionsDirection.Descend:
                    return source.OrderByDescending(expression);

                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}