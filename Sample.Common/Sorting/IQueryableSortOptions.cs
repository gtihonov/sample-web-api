using System.Linq;

namespace Sample.Common.Sorting
{
    public interface IQueryableSortOptions<TEntity, TSort> : ISortOptions<TSort> 
        where TEntity : class
        where TSort : struct
    {
        IOrderedQueryable<TEntity> SortQueryable(IQueryable<TEntity> source);
    }
}