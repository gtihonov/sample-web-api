namespace Sample.Common.Sorting
{
    public enum SortOptionsDirection
    {
        Ascend,
        Descend
    }
}