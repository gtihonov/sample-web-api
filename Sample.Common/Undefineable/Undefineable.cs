using System;
using Newtonsoft.Json;

namespace Sample.Common.Undefineable
{
    [JsonConverter(typeof(UndefineableJsonConverter))]
    public struct Undefineable<T>
    {
        private readonly bool _isDefined;
        private readonly T _value;

        public Undefineable(T value)
        {
            _isDefined = true;
            _value = value;
        }

        public bool IsDefined => _isDefined;

        public T Value
        {
            get
            {
                if (!_isDefined)
                {
                    // throw new InvalidOperationException("Value is not defined.s");
                }

                return _value;
            }
        }

        public override int GetHashCode()
        {
            return _isDefined ? _value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return _isDefined ? _value.ToString() : string.Empty;
        }

        public static implicit operator Undefineable<T>(T value)
        {
            return new Undefineable<T>(value);
        }

        public static explicit operator T(Undefineable<T> value)
        {
            if (!value.IsDefined)
            {
                throw new NotImplementedException("Value is not defined.");
            }

            return value.Value;
        }

        public void TryUpdate(Action<T> action)
        {
            if (IsDefined)
            {
                action(this.Value);
            }
        }
    }
}