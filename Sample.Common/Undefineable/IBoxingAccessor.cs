using System;

namespace Sample.Common.Undefineable
{
    public interface IBoxingAccessor
    {
        object Get(object value);
        object Set(object value);

        Type Type { get; }
    }
}