using System;

namespace Sample.Common.Undefineable
{
    public class UndefineableBoxingAccessor<T> : IBoxingAccessor
    {
        public object Get(object value)
        {
            return ((Undefineable<T>) value).Value;
        }

        public object Set(object value)
        {
            if (value == null)
            {
                return new Undefineable<T>(default);
            }

            try
            {
                T settedValue;
                if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    var underlyingType = Nullable.GetUnderlyingType(typeof(T));

                    if (underlyingType.IsEnum)
                    {
                        settedValue = (T) Enum.Parse(underlyingType, value.ToString(), true);
                    }
                    else
                    {
                        settedValue = (T)Convert.ChangeType(value, underlyingType);
                    }
                }
                else if (typeof(T).IsEnum)
                {
                    settedValue = (T) Enum.Parse(typeof(T), value.ToString(), true);
                }
                else
                {
                    settedValue = (T) Convert.ChangeType(value, typeof(T));
                }

                return new Undefineable<T>(settedValue);

            }
            catch (Exception)
            {
                return new Undefineable<T>(default);
            }
        }

        public Type Type => typeof(T);
    }
}