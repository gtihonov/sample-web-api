using System;
using Newtonsoft.Json;

namespace Sample.Common.Undefineable
{
    public class UndefineableJsonConverter : UndefineableJsonConverterBase
    {
        protected override object ReadValue(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value;
        }
    }

    public class UndefineableJsonConverter<T> : UndefineableJsonConverterBase
        where T: JsonConverter, new()
    {
        public UndefineableJsonConverter() : base()
        {

        }
        protected override object ReadValue(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var converter = new T();
            return reader.Value != null ? converter.ReadJson(reader, objectType, default, serializer) : null;
        }
    }
}