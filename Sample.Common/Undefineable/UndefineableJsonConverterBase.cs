using System;
using System.Collections.Concurrent;
using System.Linq;
using Newtonsoft.Json;

namespace Sample.Common.Undefineable
{
    public abstract class UndefineableJsonConverterBase : JsonConverter
    {
        private static readonly ConcurrentDictionary<Type, IBoxingAccessor> Accessors
            = new ConcurrentDictionary<Type, IBoxingAccessor>();

        public UndefineableJsonConverterBase() : base()
        {

        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var accessor = GetAccessor(value.GetType());
            serializer.Serialize(writer, accessor.Get(value));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var accessor = GetAccessor(objectType);
            return accessor.Set(ReadValue(reader, accessor.Type, existingValue, serializer));
        }

        public override bool CanConvert(Type objectType)
        {
            var accessor = GetAccessor(objectType);
            return accessor != null;
        }

        private IBoxingAccessor GetAccessor(Type type)
        {
            return Accessors.GetOrAdd(
                type, x =>
                {
                    var valueType = ResolveTypeParameter(type);

                    var accessor = valueType != null
                        ? Activator.CreateInstance(
                            typeof(UndefineableBoxingAccessor<>)
                                .MakeGenericType(valueType)) as IBoxingAccessor
                        : null;
                    ;

                    return accessor;
                });
        }

        protected abstract object ReadValue(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer);

        internal static Type ResolveTypeParameter(Type undefineableType)
        {
            var toCheck = undefineableType;

            while (toCheck != null && toCheck != typeof(object))
            {
                var current = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;

                if (typeof(Undefineable<>) == current)
                {
                    return toCheck.GetGenericArguments().Single();
                }

                toCheck = toCheck.BaseType;
            }

            return null;
        }
    }
}