using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Sample.Common.Paging
{
    public static class PageableReadOnlyCollectionQueryableExtensions
    {
        public static async Task<IPageableReadOnlyCollection<T>> ToPageableReadOnlyCollectionAsync<T>(
            this IQueryable<T> source, 
            int page, 
            int pageCount, 
            CancellationToken ct = default(CancellationToken))
        {

            return new PageableReadOnlyCollection<T>
            {
                TotalCount = (uint) await source.CountAsync(ct),
                Items = await source.Skip(page * pageCount).Take(pageCount).ToListAsync(ct)
            };
        }

        public static async Task<IPageableReadOnlyCollection<T>> ToPageableReadOnlyCollectionAsync<T>(
            this IQueryable<T> source, 
            IPagingOptions pagingOptions,
            CancellationToken ct = default(CancellationToken))
        {
            var totalCount = (uint) await source.CountAsync(ct);
            var items = await source.PageWithOptions(pagingOptions).ToListAsync(ct);

            return new PageableReadOnlyCollection<T>
            {
                TotalCount = totalCount,
                Page = pagingOptions.Page + 1,
                PageCount = pagingOptions.GetCurrentPageCount(),
                TotalPages = (uint)Math.Ceiling(((double)totalCount/pagingOptions.GetCurrentPageCount())),
                Items = items
            };
        }
        
    }
}