using System.Collections.Generic;

namespace Sample.Common.Paging
{
    public class PageableReadOnlyCollection<T> : IPageableReadOnlyCollection<T>
    {

        public uint TotalCount { get; set; }

        public uint Page { get; set; }

        public uint PageCount { get; set; }

        public uint TotalPages { get; set; }
        public IReadOnlyCollection<T> Items { get; set; }
    }
}