using System.Collections.Generic;

namespace Sample.Common.Paging
{
    public interface IPageableReadOnlyCollection<T>
    {
        uint TotalCount { get; set; }
        IReadOnlyCollection<T> Items { get; set; }
    }
}