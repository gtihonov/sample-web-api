using System.Linq;

namespace Sample.Common.Paging
{
    public static class PagingOptionsQueryableExtensions
    {
        public static IQueryable<T> PageWithOptions<T>(
            this IQueryable<T> source, 
            IPagingOptions options)
        {
            var pageCount = (int) options.GetCurrentPageCount();
            return source.Skip((int)options.Page * pageCount).Take(pageCount);
        }
    }
}