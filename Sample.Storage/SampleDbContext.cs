using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sample.Storage.Models.Users;

namespace Sample.Storage
{
    public class SampleDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            DateTime utcNow = DateTime.Now;
            foreach (var entry in this.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added))
            {
                if (entry.Entity is ICreatedAt createdAt)
                {
                    createdAt.CreatedAt = utcNow;
                }
                
                if (entry.Entity is IUpdatedAt updatedAt)
                {
                    updatedAt.UpdatedAt = utcNow;
                }
            }
            
            foreach (var entry in this.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified && e.Entity is IUpdatedAt))
            {
                ((IUpdatedAt) entry.Entity).UpdatedAt = utcNow;
            }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}