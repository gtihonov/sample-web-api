using System;

namespace Sample.Storage
{
    public interface ICreatedAt
    {
        DateTime CreatedAt { get; set; }
    }
}