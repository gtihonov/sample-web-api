using System;

namespace Sample.Storage
{
    public interface IUpdatedAt
    {
        DateTime UpdatedAt { get; set; }
    }
}