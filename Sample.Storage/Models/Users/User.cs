using System;

namespace Sample.Storage.Models.Users
{
    public class User : ICreatedAt, IUpdatedAt
    {
        public string Login { get; set; }
        
        public byte[] Password { get; set; }

        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}