using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Sample.Common.Paging;
using Sample.Users.Api.Commands.Create;
using Sample.Users.Api.Queries.GetList;

namespace Sample.Users.Api
{
    [ApiController, Route("api/users")]
    public class UsersApiController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public Task<IPageableReadOnlyCollection<UserDto>> GetList([FromQuery] GetUsersListQuery query)
        {
            return _mediator.Send(query);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateUserCommand command)
        {
            await _mediator.Send(command);

            return Created($"api/users/{command.Login}", null);
        }
    }
}