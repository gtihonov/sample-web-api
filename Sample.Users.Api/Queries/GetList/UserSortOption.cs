namespace Sample.Users.Api.Queries.GetList
{
    public enum UserSortOption
    {
        Login,
        CreatedAt,
        UpdatedAt
    }
}