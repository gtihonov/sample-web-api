using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Sample.Common.Paging;
using Sample.Common.Sorting;
using Sample.Storage;

namespace Sample.Users.Api.Queries.GetList
{
    public class GetUsersListQueryHandler : IRequestHandler<GetUsersListQuery, IPageableReadOnlyCollection<UserDto>>
    {
        private readonly SampleDbContext _sampleDbContext;

        public GetUsersListQueryHandler(SampleDbContext sampleDbContext)
        {
            _sampleDbContext = sampleDbContext;
        }
        public Task<IPageableReadOnlyCollection<UserDto>> Handle(GetUsersListQuery request, CancellationToken cancellationToken)
        {
            var query = _sampleDbContext.Users.AsQueryable();

            if (!String.IsNullOrWhiteSpace(request.Login))
            {
                query = query.Where(o =>
                    EF.Functions.Like(o.Login.ToUpperInvariant(), $"{request.Login.ToUpperInvariant()}%"));
            }

            return query
                .SortWithOptions(request)
                .Select(o => new UserDto()
                {
                    Login = o.Login,
                    CreatedAt = o.CreatedAt,
                    UpdatedAt = o.UpdatedAt
                })
                .ToPageableReadOnlyCollectionAsync(request, cancellationToken);
        }
    }
}