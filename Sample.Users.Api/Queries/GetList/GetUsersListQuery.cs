using System.Linq;
using MediatR;
using Sample.Common.Paging;
using Sample.Common.Sorting;
using Sample.Storage.Models.Users;

namespace Sample.Users.Api.Queries.GetList
{
    public class GetUsersListQuery : IRequest<IPageableReadOnlyCollection<UserDto>>, IPagingOptions, IQueryableSortOptions<User, UserSortOption>
    {
        public string Login { get; set; }

        public uint Page { get; set; }
        
        public uint? PageCount { get; set; }
        
        public uint MinPageCount { get; }
        
        public uint MaxPageCount { get; }
        
        public uint DefaultPageCount { get; }
        
        public UserSortOption SortBy { get; set; }
        
        public SortOptionsDirection? SortDirection { get; set; }
        
        public IOrderedQueryable<User> SortQueryable(IQueryable<User> source)
        {
            switch (SortBy)
            {
                default:
                    case UserSortOption.Login:
                    return source.OrderByWithOrderDirection(o => o.Login, SortDirection ?? SortOptionsDirection.Ascend);
                case UserSortOption.CreatedAt: 
                    return source.OrderByWithOrderDirection(o => o.CreatedAt, SortDirection ?? SortOptionsDirection.Ascend);
                case UserSortOption.UpdatedAt: 
                    return source.OrderByWithOrderDirection(o => o.UpdatedAt, SortDirection ?? SortOptionsDirection.Ascend);
            }
        }

    }
}