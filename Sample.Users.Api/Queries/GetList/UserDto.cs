using System;

namespace Sample.Users.Api.Queries.GetList
{
    public class UserDto
    {
        public string Login { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}