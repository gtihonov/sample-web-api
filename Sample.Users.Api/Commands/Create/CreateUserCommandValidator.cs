using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Sample.Storage;

namespace Sample.Users.Api.Commands.Create
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator(SampleDbContext dbContext)
        {
            RuleFor(o => o.Login)
                .MustAsync((login, ct) => dbContext.Users.AllAsync(o => o.Login != login));

            RuleFor(o => o.Password)
                .NotEmpty();
        }
    }
}