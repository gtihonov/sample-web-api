using MediatR;

namespace Sample.Users.Api.Commands.Create
{
    public class CreateUserCommand : IRequest<Unit>
    {
        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}