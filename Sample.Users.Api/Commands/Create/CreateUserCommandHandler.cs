using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Sample.Storage;
using Sample.Storage.Models.Users;

namespace Sample.Users.Api.Commands.Create
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Unit>
    {
        private readonly SampleDbContext _dbContext;
        private readonly IValidator<CreateUserCommand> _validator;

        public CreateUserCommandHandler(SampleDbContext dbContext, IValidator<CreateUserCommand> validator)
        {
            _dbContext = dbContext;
            _validator = validator;
        }
        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            await _validator.ValidateAndThrowAsync(request, cancellationToken: cancellationToken);

            using (var sha256 = SHA256.Create())
            {
                var user = new User
                {
                    Login = request.Login,
                    Password = sha256.ComputeHash(Encoding.UTF8.GetBytes(request.Password))
                };

                _dbContext.Users.Add(user);
                await _dbContext.SaveChangesAsync(cancellationToken);
            }
            
            return Unit.Value;
        }
    }
}